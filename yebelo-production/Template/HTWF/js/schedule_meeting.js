
var username;
var emailid;
var phonenumber;
var message;
var mode;
// //google ads event emitter
// function gtag_report_conversion(url) { var callback = function () { if (typeof(url) != 'undefined') {
//     //window.location = url;
//     } }; gtag('event', 'conversion', { 'send_to': 'AW-462291750/_VdxCLvK1ewBEKaGuNwB', 'event_callback': callback }); return false; }

function sendMeetingDetails() {
    
    dataLayer.push({'event': 'meeting-booked'});

    var formData = new FormData(document.querySelector('form'));
    // Sending and receiving data in JSON format using POST method
    username = document.getElementById("username").value;
    emailid = document.getElementById("emailId").value;
    localStorage.setItem('userName',document.getElementById("username").value);
    localStorage.setItem('email',document.getElementById("emailId").value);
    console.log("username :"+ username);
    var data = JSON.stringify({
        "email": "ravi@yebelo.com",
        "subject": "Yebelo Website Schedule Meeting Details",
        "body":  "<h3>Meeting Details</h3>" +" Name  : " + username +  ",<br><br>Email ID  : " + emailid
    });
    gtag_report_conversion('/enterprise-application')
    console.log(emailid);
    var xhr = new XMLHttpRequest();
    var url = "https://myclasstor.com:7100/mail";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
        }
    };
    xhr.send(data);
}

function bookSchedule() {
    var selectedDate = localStorage.getItem('selectedDate');
    if(selectedDate !== undefined){
        var data = JSON.stringify({
            "email": "ravi@yebelo.com",
            "subject": "Yebelo Website Schedule Meeting Details",
            "body":  "<h3>Meeting confirmation Details</h3>" +"<b> Name </b> : " + username +  ",<br><br><b>Email ID</b>  : " + emailid+"<br><br>"+"<b>Meeting Timings</b> : " + selectedDate
        });
        console.log(emailid);
        document.getElementById("confirmationPopup").style.display = "block";
        document.getElementById("scheduleForm").style.display = "none";
        document.getElementById("selectDate").style.display = "none";
        var xhr = new XMLHttpRequest();
        var url = "https://myclasstor.com:7100/mail";
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var json = JSON.parse(xhr.responseText);
                console.log(json);
            }
        };
        xhr.send(data);
    }
}
function selectOne(){
    localStorage.setItem('selectedDate',document.getElementById("tuesdayTime1").value);
    document.getElementById('selectedDate').innerHTML =document.getElementById("tuesdayTime1").value;
    bookSchedule();
}
function selectTwo(){
    localStorage.setItem('selectedDate',document.getElementById("thursdayTime1").value);
    document.getElementById('selectedDate').innerHTML = localStorage.getItem('selectedDate');
    bookSchedule();
}
function selectThree(){
    localStorage.setItem('selectedDate',document.getElementById("tuesdayTime2").value);
    document.getElementById('selectedDate').innerHTML = localStorage.getItem('selectedDate');
    bookSchedule();
}
function selectFour(){
    localStorage.setItem('selectedDate',document.getElementById("thursdayTime2").value);
    document.getElementById('selectedDate').innerHTML =localStorage.getItem('selectedDate');
    bookSchedule();
}
function selectFive(){
    localStorage.setItem('selectedDate',document.getElementById("tuesdayTime3").value);
    document.getElementById('selectedDate').innerHTML =localStorage.getItem('selectedDate');
    bookSchedule();
}

function toCaseStudy1(){
    window.location = '/case-study/identity-fraud-managment';
}
function toCaseStudy2(){
    window.location = '/case-study/ai-enabled-knowledge-assistant';
}
function toCaseStudy3(){
    window.location = '/case-study/conversational-Ecommerce';
}