
var username;
var emailid;
var phonenumber;
var message;
var mode;



function post_subscription_details(subscription_mode) {

    username = document.getElementById("name").value;
    console.log("username :"+ username);
    emailid = document.getElementById("email").value;
    phonenumber = document.getElementById("phone").value;
    message = document.getElementById("message").value;
    mode = subscription_mode;

    console.log("mode :"+mode);

    if (username !== "" && emailid !== "" && phonenumber !== "" && message !== ""){
        var http = new XMLHttpRequest();
        var url = "https://www.yebelo.com/yebelo_web_production/API/post_subscription_details.php?username="+username+"&emailid="+emailid+"&phonenumber="+phonenumber+"&message="+message+"&mode="+mode+"";
        http.open("POST", url, true);

        //Send the proper header information along with the request
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState === 4 && http.status === 200) {

                console.log("http" + http.responseText);

                var post_status = http.responseText;

                if (post_status === 'success'){
                    console.log("came inside success");

                    document.getElementById("success_box").style.display = "block";
                    document.getElementById("form_error_box").style.display = "none";
                    document.getElementById("error_box").style.display = "none";

                } else{
                    console.log("came inside failure");
                    document.getElementById("error_box").style.display = "block";
                    document.getElementById("form_error_box").style.display = "none";
                    document.getElementById("success_box").style.display = "none";
                }
            }
        };
        http.send();

    }  else {
        document.getElementById("form_error_box").style.display = "block";
        document.getElementById("error_box").style.display = "none";
        document.getElementById("success_box").style.display = "none";
    }
}